Contigo
-------

**¿Mi tierra?** 

Mi tierra eres tú. 

**¿Mi gente?** 

Mi gente eres tú. 

El destierro y la muerte para mi están adonde no estés tú.

 **¿Y mi vida?**


Dime, mi vida, 

**¿qué es, si no eres tú?**

*-Luis Cernuda*

<img src="MiAmorcito.jpeg" width="200" height="255"/>

